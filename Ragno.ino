#include <Servo.h>
// dal 22 al 30, motori di destra
// dal 40 al 48, motori di sinistra
// 22, 25, 28, 40, 43, 46 motori interni
// 23, 26, 29, 41, 44, 47 motori intermedi
// 24, 27, 30, 42, 45, 48 motori esterni
// in alto a destra, motore interno: senso orario, posizione normale 120, da 80 a 140
// in centro a destra, motore interno: senso orario, posizione normale 120, da 100 a 150
// in basso a destra, motore interno: senso orario, posizione normale 120, da 100 a 150
// motore medi a destra: posizione  normale 60, da 10 a 80
// motori esterni a destra; posizione normale 60, da 20 a 120

#define RIGHT_INT_DEFAULT 120
#define LEFT_INT_DEFAULT 25

#define ANGOLO_SERVO 25

//const int altezzaMaxMid[6] = {70, 80, 80, 60, 70, 60};
const int altezzaMaxExt[6] = {90, 100, 100, 40, 40, 40};
const int altezzaDefaultMid[6] = {30, 40, 40, 100, 110, 100};
const int altezzaDefaultExt[6] = {40, 50, 50, 90, 90, 90};
const int altezzaMinMid[6] = {10, 20, 20, 120, 130, 120};
const int altezzaMinExt[6] = {40, 50, 50, 90, 90, 90};


/* Possibili funzioni principali */
int inPiedi();
int aTerra();
int avanti();
int indietro();
int destra();
int sinistra();
int ferma();
int accendi();


Servo internalServo[6],
      midLegServo[6],
      externalServo[6];
int i = 0;
int val = 2;
int actualPos[6] = {RIGHT_INT_DEFAULT-10, RIGHT_INT_DEFAULT, RIGHT_INT_DEFAULT, LEFT_INT_DEFAULT+10, LEFT_INT_DEFAULT, LEFT_INT_DEFAULT-10};

void setup() {
  Serial.begin(9600);
 // for(i = 0; i < 3; i++) {
      internalServo[0].attach(40);
      internalServo[1].attach(43);
      internalServo[2].attach(46);
      midLegServo[0].attach(41);
      midLegServo[1].attach(44);
      midLegServo[2].attach(47);
      externalServo[0].attach(42);
      externalServo[1].attach(45);
      externalServo[2].attach(48);
      
      internalServo[3].attach(22);
      internalServo[4].attach(25);
      internalServo[5].attach(28);
      midLegServo[3].attach(23);
      midLegServo[4].attach(26);
      midLegServo[5].attach(29);
      externalServo[3].attach(24);
      externalServo[4].attach(27);
      externalServo[5].attach(30);

      internalServo[0].write(RIGHT_INT_DEFAULT-10);
      internalServo[1].write(RIGHT_INT_DEFAULT);
      internalServo[2].write(RIGHT_INT_DEFAULT);
      internalServo[3].write(LEFT_INT_DEFAULT+10);
      internalServo[4].write(LEFT_INT_DEFAULT);
      internalServo[5].write(LEFT_INT_DEFAULT-10); 
 // }
}

void loop() {
 char value[5];
 while (Serial.available()) {
  char ch = Serial.read();
  if (i < 5) {
    value[i] = ch;
    i++;
  }
  else {
    break;
  }
 }
 if (i > 0) {
  i = 0;
  val = atoi(value);
  Serial.println(val);
 }
 if (val == 1){
    muovi(1);
 } else if (val == 2) {
    inPiedi();
 } else if (val == 3) {
    aTerra();
 } else if (val == 4) {
    muovi(2);
 }

 delay(300);
}

int muovi(int verso) {
  int gamba = 0;
  int mov2 = 0;
  int atto = 0;
  int i;
  bool gambaAttiva[6] = {false, false, false, false, false, false};
  for (i = 0; i < 6; i++) {
    internalServo[i].write(actualPos[i]);
  }
  unsigned long int tempo = millis();  
  unsigned long int tempo2 = millis();
  while(1) {
    if (atto == 0) {
      gambaAttiva[gamba] = true;
      atto = 1;
    }
    if (millis() - tempo >= 10) {
      passo(gamba, verso, atto, mov2);
      mov2++;
      if (mov2 == ANGOLO_SERVO) {
        mov2 = 0;
        atto++;
        if (atto > 3) {
          atto = 0;
          gambaAttiva[gamba] = false;
          gamba++;
          if (gamba == 6) gamba = 0;
        }
      }
      tempo = millis();
    }
    
    if (millis() - tempo2 >= 120) {
      int i;
      for (i = 0; i < 6; i++) {
        if (gambaAttiva[i] == false) {
          internalServo[i].write(actualPos[i]);
          if ((i < 3 && actualPos[i] < 140 && verso == 1) || ( i > 2 && actualPos[i] > 10 && verso == 2)) {
            actualPos[i]++;
          } else if ((i > 2 && actualPos[i] > 10 && verso == 1) || (i < 3 && actualPos[i] < 140 && verso == 2)) {
            actualPos[i]--;
          }
        }
      }
     tempo2 = millis();
    }
  }
}

void passo(int gamba, int verso, int atto, int mov2) {
  switch (atto) {
    case 1:
      if (gamba < 3) {
      midLegServo[gamba].write(30-mov2);
      } else {
        midLegServo[gamba].write(100+mov2);
      }
      break;
    case 2:
      if (verso == 1) {
        if (gamba<3) actualPos[gamba]--;
        else actualPos[gamba]++;
      } else {
        if (gamba<3) actualPos[gamba]++;
        else actualPos[gamba]--;
      }
      internalServo[gamba].write(actualPos[gamba]);
      mov2++;
      break;
   case 3: 
      if (gamba < 3) {
        midLegServo[gamba].write(10+mov2);
      } else {
        midLegServo[gamba].write(120-mov2);
      }
      break;
  }
}

int aTerra() {
  int i;

  for (i = 0; i < 6; i++) {
    externalServo[i].write(altezzaMinExt[i]);
    midLegServo[i].write(altezzaMinMid[i]);
  }
  delay(300);
}

int inPiedi() {
  int i;
  for (i = 0; i < 6; i++) {
    externalServo[i].write(altezzaDefaultExt[i]);
    midLegServo[i].write(altezzaDefaultMid[i]);
  }
  delay(300);
}

