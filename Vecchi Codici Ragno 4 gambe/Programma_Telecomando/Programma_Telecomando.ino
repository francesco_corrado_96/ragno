
int Modalita = 0;
int X = 1;
int MovimentoVert = A0;
int MovimentoOr = A1;
int PulsSinistra = 6;
int PulsDestra = 5;
int A = A2;
byte Ampiezza = 27;
byte ContrAmp;
int T = A3;
int Tempo = 2000;
int ContrTemp;
int LedGiallo = 2;
int LedRosso = 3;
int LedVerde = 4;

union BIDATO{
  int i;
  byte b[2];
  };
BIDATO C;



void setup() {
  Serial.begin (9600);
  Serial1.begin(9600);
  pinMode (2, OUTPUT);
  pinMode (3, OUTPUT);
  pinMode (4, OUTPUT);
  pinMode (5, INPUT);
  pinMode (6, INPUT);

}

void loop() {
 
  
  if (Modalita == 0)
  {
    digitalWrite(LedGiallo, LOW);
    digitalWrite(LedVerde, LOW);
    digitalWrite(LedRosso, HIGH);
  }
  if (Modalita == 1)
  {
    digitalWrite(LedGiallo, HIGH);
    digitalWrite(LedVerde, LOW);
    digitalWrite(LedRosso, LOW);
  }
  if (Modalita == 2 || Modalita == 3 || Modalita == 4 || Modalita == 5)
  {
    digitalWrite(LedGiallo, LOW);
    digitalWrite(LedVerde, HIGH);
    digitalWrite(LedRosso, LOW);
  }
  
  
    
  if (analogRead(MovimentoVert) >= 1000  && Modalita != 2 && X == 0)
  {
    while(analogRead(MovimentoOr) < 20 || analogRead(MovimentoOr) > 1000)
    {
      if (Modalita != 1)
      {
      trasmitRequest(1,0,0);
      Modalita = 1;
      }
      digitalWrite(LedVerde, LOW);
      digitalWrite(LedRosso, HIGH);
      delay(400);
      digitalWrite(LedRosso, LOW);
      delay(400);
    }
    trasmitRequest(2,0,0);
    Modalita = 2;
  }
  if (analogRead(MovimentoVert) <=20&& Modalita != 5 && X == 0)
  {
     while(analogRead(MovimentoOr) < 20 || analogRead(MovimentoOr) > 1000)
    {
      if (Modalita != 1)
      {
      trasmitRequest(1,0,0);
      Modalita = 1; 
      }     
      digitalWrite(LedVerde, LOW);
      digitalWrite(LedRosso, HIGH);
      delay(400);
      digitalWrite(LedRosso, LOW);
      delay(400);
    }
    trasmitRequest(5,0,0);
    Modalita = 5;
  }
  if (analogRead(MovimentoOr) <= 20 && Modalita != 3 && X == 0)
  {
    trasmitRequest(3,0,0);
    Modalita = 3;
  }
  if (analogRead(MovimentoOr) >= 1000 && Modalita != 4 && X == 0)
  {
    trasmitRequest(4,0,0);
    Modalita = 4;
  }
  
  
  if (Modalita != 1 && X == 0)
  {
  if(analogRead(MovimentoVert) <=950 && analogRead(MovimentoVert) >= 450)
   {
    if(analogRead(MovimentoOr) <=950 && analogRead(MovimentoOr) >= 450)
     {
     trasmitRequest(1,0,0);
     Modalita = 1;
     }
   }
  }
   
   
  if (digitalRead(PulsSinistra) == 0)
  {
     if (X == 0)
     {
      trasmitRequest (0,0,0);
      Modalita = 0; 
      delay(300);
    }
     if (X == 1)
     {
       trasmitRequest(1,1,0);
       Modalita = 1;
       delay(300);
     }
    X = !X;   
  }
  
  if (digitalRead(PulsDestra) == 0)
  {
    
    if (X == 0)
     {
      trasmitRequest (0,0,0);
      Modalita = 0; 
      delay(300);
    }
     if (X == 1)
     {
       trasmitRequest (12,0,0);
       Modalita = 12;
       delay(300);
     }
    X = !X; 
  }
  
  
  
 /*
  if (analogRead(A) >=1000)
  {
    Ampiezza++;
    if (Ampiezza >=30) {Ampiezza = 30;}
    delay(100);
  }

  if (analogRead(A) <= 20)
    {
    Ampiezza --;
    if (Ampiezza <=7) {Ampiezza = 7;}
    delay(100);
    }
   
  if (ContrAmp != Ampiezza)
   {
    trasmitRequest(6,Ampiezza,0);
    Serial.println("INVIO7");
  }
    
    if (analogRead(T) >= 1000)
    {
      Tempo = Tempo + 100;
      if(Tempo >= 4000){Tempo = 4000;}
      delay(100);
    }
    
    if (analogRead(T) <= 20)
   {
      Tempo = Tempo - 100;
      if (Tempo <= 500) {Tempo = 500;}
      delay(100);
   }
    
   if (ContrTemp != Tempo)
   {
     C.i = Tempo;
     Serial.println("INVIO8");
     trasmitRequest(7,C.b[0],C.b[1]);
   }
   */
  
}

void ricevi(){
    
  byte lunM,
       lunL,
       checksum,
       datArray[30];
       
  int lunghezza; 
 if(Serial1.available()>=1) {
      
  if(Serial1.read()==0x7E) {
    while(Serial1.available() <2){  
    }      
    lunM = Serial1.read();
    lunL = Serial1.read();
    lunghezza=(int)(lunL+(lunM<<8));       
    while(Serial1.available() <lunghezza){  
    }        
    for(int i=0;i<lunghezza;i++){
      datArray[i]=Serial1.read();
    }    
    while(Serial1.available() <1){  
    }
    checksum=Serial1.read();
    
    switch(datArray[0]) {   
      case 0x8B:
      Serial.println("Comando Inviato");    // Zig Bee Trasmit Status p. 111  
        break;      
      case 0x90:    // ZigBee Receive Packet p. 112
        break;
   }
  }
}
}




void trasmitRequest(byte v1,byte v2, byte v3){

  
  Serial1.write(0x7E);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x16);
  Serial1.write((byte)0x10);
  Serial1.write((byte)0x01);  
  Serial1.write((byte)0x00);
  Serial1.write((byte)0x13);
  Serial1.write((byte)0xA2);
  Serial1.write((byte)0x00); 
  Serial1.write((byte)0x40);
  Serial1.write((byte)0xB4); 
  Serial1.write((byte)0x09); 
  Serial1.write((byte)0xB0); 
  Serial1.write((byte)0xFF); 
  Serial1.write((byte)0xFE);  
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)v1);
  Serial1.write((byte)v2);
  Serial1.write((byte)v3);
  Serial1.write((byte)0); 
  Serial1.write((byte)0);
  Serial1.write((byte)0); 
  Serial1.write((byte)0); 
  Serial1.write((byte)0); 
 
  int sum=0x10+0x01+0x00+0x13+0xA2+0x00+0x40+0xB4+0x09+0xB0+0xFF+(0xFE)+v1+v2+v3;

  Serial1.write((byte)(0xFF-(sum&0xFF)));

  delay(100);
  
}
