// SPIDER ARDUINO
// by Segatello Mirco for ElettronicaIN 
// Compilato con Arduino21

#include <Servo.h> 
#include <IRremote.h> 

#define RECV_PIN 10   // pin per sensore IR
#define PING_PIN 11   // pin per sensore RADAR
#define       P1 12   // pin per pulsante

#define STNB_CODE1 0x81D    // Stop Philips TV remote
#define STNB_CODE2 0x1D     // Codice alternativo
#define WALK_CODE1 0x81C    // Play Philips TV remote
#define WALK_CODE2 0x1C     // Codice alternativo
#define SPEEDUP_CODE1 0x820 // Prg+ Philips TV remote
#define SPEEDUP_CODE2 0x20  // Codice alternativo
#define SPEEDDW_CODE1 0x821 // Prg- Philips TV remote
#define SPEEDDW_CODE2 0x21  // Codice alternativo
#define LEFT_CODE1    0x2C  // Left Philips remote
#define LEFT_CODE2    0x82C  // Codice alternativo
#define RIGHT_CODE1    0x2B  // Left Philips remote
#define RIGHT_CODE2    0x82B  // Codice alternativo

#define MODE_OFF       0    // Servi OFF
#define MODE_STANDBY   1    // Servi armati in posizione di riposo
#define MODE_INWALK    2    // Assume la posizione di partenza
#define MODE_WALK      3    // Cammina in avanti
#define MODE_INSTANDBY 4    // Assume la posizione di standby
#define MODE_LEFT      5    // Si gira verso sinistra
#define MODE_RIGHT     6    // Si gira a destra
#define MODE_REVWALK   7    // Cammina all'indietro

IRrecv irrecv(RECV_PIN);
decode_results results;
                     
int RobotMode = MODE_OFF;   
int TimeOneStep = 6000;     // Tempo in msec per eseguire un passo completo
 
Servo servo0, servo1, servo2, servo3,
      servo4, servo5, servo6, servo7;  // oggetti servo

int leg_old[8] = {90, 90, 90, 90, 90, 90, 90, 90}; //posizione precedente dei servi
int leg[8];                 //posizione attuale dei servi
int i;
 
void setup() 
{ 
  Serial.begin(9600);       // Start seriale
  Serial.println("BIPE Aduino V1.0");
  irrecv.enableIRIn();      // Start ricevitore IR  
  irrecv.blink13(true);     // Lampeggio LED in ricezione IR attivo
  pinMode(P1, INPUT);       // Pulsante 
  digitalWrite(P1, HIGH);   // Abilita pull-up
  levelBat();               // Verifica stato batteria  
  Serial.println("Livello batteria OK.");  
  Serial.println("Inizializzo Servi...");  
  initServo();              // Inizializza servi
  Serial.println("Servi inizializzati.");
  inStandby();              // Robot in attesa di comando
} 
  
void loop() 
{ 
  if (irrecv.decode(&results)) IRcommand();
  if (RobotMode==MODE_WALK)   walk();
  if (RobotMode==MODE_REVWALK) walkReverse();
  if (RobotMode==MODE_LEFT)   leftWalk();  
  if (RobotMode==MODE_RIGHT)   rightWalk();
  levelBat();  
  if (RobotMode==MODE_STANDBY) delay(100); 
  if (digitalRead(P1)==0) P1Press();  
  
  int obstacle = readDistance();
  Serial.print("Ostacolo: ");
  Serial.println(obstacle, DEC);    
  if (obstacle<20)  inStandby();    
  
  // Ricezione seriale per setup servi
  if (Serial.available()) SerialSet();  
} 

void SerialSet()
{
   // E' arrivato un comando dalla seriale
  byte c;
  byte srv;
  
  while(Serial.available())     // finchè ci sono dati 
  {
      c = Serial.read();    // legge i dati e li salva in una variabile
      if ( 97 <= c <= 102)   Serial.print(c);
 }  
}

void IRcommand()
{
    // E' arrivato un comando via IR
    Serial.println(results.value, HEX);  // ECO su serial monitor per sapere il codice arrivato
    
    if (results.value == WALK_CODE1 || results.value == WALK_CODE2)   inWalk();     
    else if (results.value == LEFT_CODE1 || results.value == LEFT_CODE2)  inLeft();     
    else if (results.value == RIGHT_CODE1 || results.value == RIGHT_CODE2)  inRight();       
    else if (results.value == STNB_CODE1 || results.value == STNB_CODE2)  inStandby();            
    else if (results.value == SPEEDUP_CODE1 || results.value == SPEEDUP_CODE2) {
      if (TimeOneStep > 1000) TimeOneStep -= 500; }
    else if (results.value == SPEEDDW_CODE1 || results.value == SPEEDDW_CODE2) {
      if (TimeOneStep < 6000) TimeOneStep += 500; }
    Serial.print("TimeOneStep= ");      
    Serial.println(TimeOneStep);
    irrecv.resume(); // Resume decoding   
}  
  
void P1Press()
{
  // E' stato premuto il pulsante   
  while (digitalRead(P1)==0) delay(100);  
  if (RobotMode==MODE_WALK)  inStandby();            
  else if (RobotMode==MODE_STANDBY) inWalk(); 
}  

void levelBat()
{
  // controlla livello batteria  
  int VbatRAW = analogRead(A0);
  float Vbat = float(VbatRAW)/50;
  Serial.print("Vbat= ");
  Serial.println(Vbat, DEC);     
  if (Vbat<6.0)   {
    Serial.println("Livello batteria basso!");    
    inOff();    
  }  
}

void initServo()  
{
  // posizione iniziale dei servi
  servo0.attach(2);  // connette servo 
  delay(300);
  servo1.attach(3);  // connette servo 
  delay(300);  
  servo2.attach(4);  // connette servo 
  delay(300);
  servo3.attach(5);  // connette servo 
  delay(300);  
  servo4.attach(6);  // connette servo 
  delay(300);
  servo5.attach(7);  // connette servo 
  delay(300);  
  servo6.attach(8);  // connette servo 
  delay(300);
  servo7.attach(9);  // connette servo 
  delay(300);     
}

void inOff()
{
  // Spegne tutti i servi
  leg[0] = 90;
  leg[1] = 90;
  leg[2] = 90;
  leg[3] = 90;
  leg[4] = 90;
  leg[5] = 90;
  leg[6] = 90;
  leg[7] = 90;      
  setServo();  
  servo0.detach();  // connette servo 
  servo1.detach();  // connette servo 
  servo2.detach();  // connette servo 
  servo3.detach();  // connette servo 
  servo4.detach();  // connette servo 
  servo5.detach();  // connette servo 
  servo6.detach();  // connette servo 
  servo7.detach();  // connette servo 
  RobotMode = MODE_OFF;     
  // Da questo modalità si deve spegnere il robot e ricaricare le batterie
}  

void inStandby()
{
  // Posiziono tutti i servi al centro
  leg[0] = 90;
  leg[1] = 90;
  leg[2] = 90;
  leg[3] = 90;
  leg[4] = 90;
  leg[5] = 90;
  leg[6] = 90;
  leg[7] = 90;      
  setServo();  
  RobotMode = MODE_STANDBY;  
  Serial.println("Sono in Standby...");  
}  

void inWalk()
{
  // Si alza pronto per partire   
  leg[0] = 99;
  leg[1] = 126;
  leg[2] = 117;
  leg[3] = 54;
  leg[4] = 99;
  leg[5] = 126;
  leg[6] = 117;
  leg[7] = 54;      
  setServo();  
  RobotMode = MODE_WALK;
  Serial.println("Walk Mode...");   
}

void setServo() 
{  
   // Posiziona i servi alle cordinate specificate da leg[]  
   for (i=0; i<18; i++) {
       servo0.write(leg_old[0] + i*(leg[0]-leg_old[0])/18);  
       servo1.write(leg_old[1] + i*(leg[1]-leg_old[1])/18);        
       servo2.write(leg_old[2] + i*(leg[2]-leg_old[2])/18);  
       servo3.write(leg_old[3] + i*(leg[3]-leg_old[3])/18);      
       servo4.write(leg_old[4] + i*(leg[4]-leg_old[4])/18);  
       servo5.write(leg_old[5] + i*(leg[5]-leg_old[5])/18);        
       servo6.write(leg_old[6] + i*(leg[6]-leg_old[6])/18);  
       servo7.write(leg_old[7] + i*(leg[7]-leg_old[7])/18);    
       delay(TimeOneStep/4/18);
    }   
    leg_old[0]=leg[0];
    leg_old[1]=leg[1]; 
    leg_old[2]=leg[2];
    leg_old[3]=leg[3];  
    leg_old[4]=leg[4];
    leg_old[5]=leg[5]; 
    leg_old[6]=leg[6];
    leg_old[7]=leg[7];       
}  

void setServoWalk() 
{     
   // Posiziona i servi durante la camminata 
   for (i=1; i<=18; i++) {
       servo0.write(leg_old[0] + i*(leg[0]-leg_old[0])/18);          
       servo2.write(leg_old[2] + i*(leg[2]-leg_old[2])/18);       
       servo4.write(leg_old[4] + i*(leg[4]-leg_old[4])/18);       
       servo6.write(leg_old[6] + i*(leg[6]-leg_old[6])/18);  

      // servi delle zampe
         if ( i <= 9 ) 
           servo1.write(leg_old[1] + i*(leg[1]-leg_old[1])/9);         
         else  
           servo1.write(leg_old[1] + (18-i)*(leg[1]-leg_old[1])/9);      

         if ( i <= 9 ) 
           servo3.write(leg_old[3] + i*(leg[3]-leg_old[3])/9);                   
         else    
           servo3.write(leg_old[3] + (18-i)*(leg[3]-leg_old[3])/9); 
            
         if ( i <= 9 ) 
           servo5.write(leg_old[5] + i*(leg[5]-leg_old[5])/9);         
         else  
           servo5.write(leg_old[5] + (18-i)*(leg[5]-leg_old[5])/9);    
           
         if ( i <= 9 ) 
           servo7.write(leg_old[7] + i*(leg[7]-leg_old[7])/9);         
         else  
           servo7.write(leg_old[7] + (18-i)*(leg[7]-leg_old[7])/9);       
       
       delay(TimeOneStep/4/18);
    }   
    leg_old[0]=leg[0];
    leg_old[1]=126;//leg[1]; 
    leg_old[2]=leg[2];
    leg_old[3]=54;//leg[3];  
    leg_old[4]=leg[4];
    leg_old[5]=126;//leg[5]; 
    leg_old[6]=leg[6];
    leg_old[7]=54; //leg[7];  
    if (digitalRead(P1)==0) P1Press();  
    if (irrecv.decode(&results)) IRcommand();    
}  

void walk()
{
      // Coordinate per eseguire un passo
      
      //passo 1      
      leg[0] = 117;
      leg[1] = 126;
      leg[2] = 63;
      leg[3] = 90;   //posizione zampa alzata
      leg[4] = 81;
      leg[5] = 126;
      leg[6] = 99;
      leg[7] = 54;  
      setServoWalk();      
      if (RobotMode!=MODE_WALK) return;
      
      //passo 2      
      leg[0] = 63;
      leg[1] = 90;   // posizione zampa alzata
      leg[2] = 81;
      leg[3] = 54;
      leg[4] = 63;
      leg[5] = 126;
      leg[6] = 81;
      leg[7] = 54;  
      setServoWalk();
      if (RobotMode!=MODE_WALK) return;
      
      //passo 3      
      leg[0] = 81;
      leg[1] = 126;
      leg[2] = 99;
      leg[3] = 54;
      leg[4] = 117;
      leg[5] = 90;    // posizione zampa alzata
      leg[6] = 63;
      leg[7] = 54;  
      setServoWalk();
      if (RobotMode!=MODE_WALK) return; 
      
      //passo 4      
      leg[0] = 99;
      leg[1] = 126;
      leg[2] = 117;
      leg[3] = 54;
      leg[4] = 99;
      leg[5] = 126;
      leg[6] = 117;
      leg[7] = 90;    // posizione zampa alzata
      setServoWalk();       
}

void walkReverse()
{
      //passo 1      
      leg[0] = 63;
      leg[1] = 126;
      leg[2] = 117;
      leg[3] = 90;  // si alza
      leg[4] = 99;
      leg[5] = 126;
      leg[6] = 81;
      leg[7] = 54;  
      setServoWalk();      
      if (RobotMode!=MODE_WALK) return;   
      
      //passo 2      
      leg[0] = 117;
      leg[1] = 126;
      leg[2] = 99;
      leg[3] = 54;
      leg[4] = 117;
      leg[5] = 90;  // si alza
      leg[6] = 99;
      leg[7] = 54;  
      setServoWalk();
      if (RobotMode!=MODE_WALK) return;
      
      //passo 3      
      leg[0] = 99;
      leg[1] = 126;
      leg[2] = 81;
      leg[3] = 54;
      leg[4] = 63;
      leg[5] = 126;
      leg[6] = 117;
      leg[7] = 90;  // si alza  
      setServoWalk();
      if (RobotMode!=MODE_WALK) return; 
      
      //passo 4      
      leg[0] = 81;
      leg[1] = 90; // si alza
      leg[2] = 63;
      leg[3] = 54;
      leg[4] = 81;
      leg[5] = 126;
      leg[6] = 63;
      leg[7] = 54;  
      setServoWalk();       
}

void inLeft()
{
  // Si alza pronto per girare a sinistra   
  leg[0] = 63;
  leg[1] = 126;
  leg[2] = 81;
  leg[3] = 54;
  leg[4] = 99;
  leg[5] = 126;
  leg[6] = 117;
  leg[7] = 54;      
  setServo();  
  RobotMode = MODE_LEFT;
  Serial.println("Left Mode...");   
}

void leftWalk()
{
      //passo 1      
      leg[0] = 81;
      leg[1] = 126;
      leg[2] = 99;
      leg[3] = 54;  
      leg[4] = 117;
      leg[5] = 126;
      leg[6] = 63;
      leg[7] = 90;//*  
      setServoWalk();      
      if (RobotMode!=MODE_LEFT) return;   
      
      //passo 2      
      leg[0] = 99;
      leg[1] = 126;
      leg[2] = 117;
      leg[3] = 54;
      leg[4] = 63;
      leg[5] = 90;//*
      leg[6] = 99;
      leg[7] = 54;  
      setServoWalk();
      if (RobotMode!=MODE_LEFT) return;
      
      //passo 3      
      leg[0] = 117;
      leg[1] = 126;
      leg[2] = 63;
      leg[3] = 90;//*
      leg[4] = 81;
      leg[5] = 126;
      leg[6] = 99;
      leg[7] = 54;   
      setServoWalk();
      if (RobotMode!=MODE_LEFT) return; 
      
      //passo 4      
      leg[0] = 63;
      leg[1] = 90;//*
      leg[2] = 81;
      leg[3] = 54;
      leg[4] = 99;
      leg[5] = 126;
      leg[6] = 117;
      leg[7] = 54;  
      setServoWalk();         
}

void inRight()
{
  // Si alza pronto per girare a destra   
  leg[0] = 99;
  leg[1] = 126;
  leg[2] = 117;
  leg[3] = 54;
  leg[4] = 63;
  leg[5] = 126;
  leg[6] = 81;
  leg[7] = 54;      
  setServo();  
  RobotMode = MODE_RIGHT;
  Serial.println("Right Mode...");   
}

void rightWalk()
{
      //passo 1      
      leg[0] = 81;
      leg[1] = 126;
      leg[2] = 99;
      leg[3] = 54;  
      leg[4] = 117;
      leg[5] = 90;//*
      leg[6] = 63;
      leg[7] = 54;  
      setServoWalk();      
      if (RobotMode!=MODE_RIGHT) return;   
      
      //passo 2      
      leg[0] = 63;
      leg[1] = 126;
      leg[2] = 81;
      leg[3] = 54;
      leg[4] = 99;
      leg[5] = 126;
      leg[6] = 117;
      leg[7] = 90;//*  
      setServoWalk();
      if (RobotMode!=MODE_RIGHT) return;
      
      //passo 3      
      leg[0] = 117;
      leg[1] = 90;//*
      leg[2] = 63;
      leg[3] = 54;
      leg[4] = 81;
      leg[5] = 126;
      leg[6] = 99;
      leg[7] = 54;   
      setServoWalk();
      if (RobotMode!=MODE_RIGHT) return; 
      
      //passo 4      
      leg[0] = 99;
      leg[1] = 126;
      leg[2] = 117;
      leg[3] = 90;//*
      leg[4] = 63;
      leg[5] = 126;
      leg[6] = 81;
      leg[7] = 54;  
      setServoWalk();           
} 


long readDistance()
{
  // Procedura per leggere la presenza di ostacoli
  // Viene generato un PING di 2usec ed atteso l'ECO di risposta
  long duration, cm;
  pinMode(PING_PIN, OUTPUT);
  digitalWrite(PING_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(PING_PIN, HIGH);
  delayMicroseconds(5);
  digitalWrite(PING_PIN, LOW);

  // Lo stesso pin che genera il trigger è usato per la lettura
  // del segnale di ritorno
  pinMode(PING_PIN, INPUT);
  duration = pulseIn(PING_PIN, HIGH);

  // ritorna distanza ostacolo in cm
  return duration / 29 / 2;
}

