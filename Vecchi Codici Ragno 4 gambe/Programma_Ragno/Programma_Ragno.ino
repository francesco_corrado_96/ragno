#include <Servo.h> 

 
Servo servo0, servo1, servo2, servo3,
      servo4, servo5, servo6, servo7; 
      



byte MODALITA = 0;
byte MODALITA2;
int i;

int pos[] = {90,90,90,90,90,90,90,90};
int posVecchia[] = {90,90,90,90,90,90,90,90};
int Ampiezza = 27;
int Tempo = 600;
int TempoSeduto = 3500;

union BIDATO{
  int i;
  byte b[2];
  };
BIDATO C;

void setup() 
{ 
  Serial1.begin(9600);
  Serial.begin(9600);
  servo0.attach(2);  
  servo1.attach(3);  
  servo2.attach(4);  
  servo3.attach(5);  
  servo4.attach(6);  
  servo5.attach(7); 
  servo6.attach(8);  
  servo7.attach(9);
} 
 
void loop() 
{  
    ricevi();
    //riceviSeriale();
    if (MODALITA == 0) { Seduto(); }
    if (MODALITA == 1) { inStandby(); }
    if (MODALITA == 2) { walk(); }
    if (MODALITA == 3) { rightWalk(); }
    if (MODALITA == 4) { leftWalk(); }
    if (MODALITA == 5) { walkReverse(); }
    
    

 
} 

/*
void riceviSeriale()
{
  if(Serial.available()>0)
  {
    switch(Serial.read()){
          case 'f':
              MODALITA = 0;           
              break;
              
          case 's':
              MODALITA = 1;
              break;
              
          case '8':
              inWalk();
              break;
              
          case '6':
              inRight();
              break;
              
          case '4':
             inLeft(); 
             break;
             
          case '2':
             inReverseWalk();
             break;
        
          case 'a':
          
              while (Serial.available() < 2){}
              byte X;
              byte Y;
              X = Serial.read()-48;
              Y = Serial.read()-48;
              Ampiezza=(int)(X*10+Y);
              break;
              
         case 't':
         
              while (Serial.available() < 4){}
              X = Serial.read()-48;
              Y = Serial.read()-48;
              byte Z = Serial.read()-48;
              byte L = Serial.read()-48;
              Tempo=(int)(X*1000+Y*100+Z*10+L);
              break;
      
  }
}
}
  
*/

void Seduto()
{
  pos[0] = 90;
  pos[1] = 90;
  pos[2] = 90;
  pos[3] = 90;
  pos[4] = 90;
  pos[5] = 90;
  pos[6] = 90;
  pos[7] = 90;      
  Esecuzione2(); 
  MODALITA = 0;
}

void inGinocchio()
{
  pos[0] = 90;
  pos[1] = 90;
  pos[2] = 90;
  pos[3] = 54;
  pos[4] = 90;
  pos[5] = 126;
  pos[6] = 90;
  pos[7] = 90;      
  Esecuzione2(); 
  MODALITA = 7;  
}  


void inPiedi()
{
  pos[0] = 90;
  pos[1] = 126;
  pos[2] = 90;
  pos[3] = 54;
  pos[4] = 90;
  pos[5] = 126;
  pos[6] = 90;
  pos[7] = 54;
  Esecuzione2();
  MODALITA = 1;
}
  
void inStandby()
{
  // Posiziono tutti i servi al centro
  pos[0] = posVecchia[0];
  pos[1] = posVecchia[1];
  pos[2] = posVecchia[2];
  pos[3] = posVecchia[3];
  pos[4] = posVecchia[4];
  pos[5] = posVecchia[5];
  pos[6] = posVecchia[6];
  pos[7] = posVecchia[7];      
  Esecuzione();
  MODALITA = 1;  
}

void inWalk()
{
  // Si alza pronto per partire   
  pos[0] = 99;
  pos[1] = 126;
  pos[2] = 117;
  pos[3] = 54;
  pos[4] = 99;
  pos[5] = 126;
  pos[6] = 117;
  pos[7] = 54;      
  Esecuzione(); 
  MODALITA = 2; 
   
}
void inRight()
{
  // Si alza pronto per girare a destra   
  pos[0] = 99;
  pos[1] = 126;
  pos[2] = 117;
  pos[3] = 54;
  pos[4] = 63;
  pos[5] = 126;
  pos[6] = 81;
  pos[7] = 54;      
  Esecuzione();  
  MODALITA = 3;   
}

void inLeft()
{
  // Si alza pronto per girare a sinistra   
  pos[0] = 63;
  pos[1] = 126;
  pos[2] = 81;
  pos[3] = 54;
  pos[4] = 99;
  pos[5] = 126;
  pos[6] = 117;
  pos[7] = 54;      
  Esecuzione();
  MODALITA = 4;  
}

void inReverseWalk()
{
  // Si alza pronto per partire   
  pos[0] = 99;
  pos[1] = 126;
  pos[2] = 117;
  pos[3] = 54;
  pos[4] = 99;
  pos[5] = 126;
  pos[6] = 117;
  pos[7] = 54;      
  Esecuzione(); 
  MODALITA = 5; 
   
}

void walkReverse()
{

      pos[0] = 81;
      pos[1] = 126;
      pos[2] = 99;
      pos[3] = 54;
      pos[4] = 90 + Ampiezza;
      pos[5] = 126;
      pos[6] = 90 - Ampiezza;
      pos[7] = 90;
      setServoWalk(); 
      if (MODALITA != MODALITA2) { return; }     

  
      pos[0] = 90 - Ampiezza;
      pos[1] = 126;
      pos[2] = 81;
      pos[3] = 54;
      pos[4] = 90 - Ampiezza; 
      pos[5] = 90;
      pos[6] = 81; 
      pos[7] = 54;
      setServoWalk();
      if (MODALITA != MODALITA2) { return; } 
     
      pos[0] = 90 + Ampiezza;
      pos[1] = 90;
      pos[2] = 90 - Ampiezza;
      pos[3] = 54;
      pos[4] = 81;
      pos[5] = 126;
      pos[6] = 99;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; } 
      
      pos[0] = 99;
      pos[1] = 126;
      pos[2] = 90 + Ampiezza;
      pos[3] = 90;
      pos[4] = 99;
      pos[5] = 126;
      pos[6] = 90 + Ampiezza;
      pos[7] = 54;
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }  
         
}
void walk()
{
      // Coordinate per eseguire un passo
 
      //passo 1     
      pos[0] = 90 + Ampiezza;
      pos[1] = 126;
      pos[2] = 90 - Ampiezza;
      pos[3] = 90;   //posizione zampa alzata
      pos[4] = 81;
      pos[5] = 126;
      pos[6] = 99;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }      

 
      //passo 2      
      pos[0] = 90 - Ampiezza;
      pos[1] = 90;   // posizione zampa alzata
      pos[2] = 81;
      pos[3] = 54;
      pos[4] = 90 - Ampiezza;
      pos[5] = 126;
      pos[6] = 81;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }

 
      //passo 3      
      pos[0] = 81;
      pos[1] = 126;
      pos[2] = 99;
      pos[3] = 54;
      pos[4] = 90 + Ampiezza;
      pos[5] = 90;    // posizione zampa alzata
      pos[6] = 90 - Ampiezza;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }

 
      //passo 4      
      pos[0] = 99;
      pos[1] = 126;
      pos[2] = 90 + Ampiezza;
      pos[3] = 54;
      pos[4] = 99;
      pos[5] = 126;
      pos[6] = 90 + Ampiezza;
      pos[7] = 90;    // posizione zampa alzata
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }      
}

void leftWalk()
{
      //passo 1      
      pos[0] = 81;
      pos[1] = 126;
      pos[2] = 99;
      pos[3] = 54;  
      pos[4] = 90 + Ampiezza;
      pos[5] = 126;
      pos[6] = 90 - Ampiezza;
      pos[7] = 90;//*  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }      
 
      //passo 2      
      pos[0] = 99;
      pos[1] = 126;
      pos[2] = 90 + Ampiezza;
      pos[3] = 54;
      pos[4] = 90 - Ampiezza;
      pos[5] = 90;//*
      pos[6] = 99;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }
   
      //passo 3      
      pos[0] = 90 + Ampiezza;
      pos[1] = 126;
      pos[2] = 90 - Ampiezza;
      pos[3] = 90;//*
      pos[4] = 81;
      pos[5] = 126;
      pos[6] = 99;
      pos[7] = 54;   
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }

      //passo 4      
      pos[0] = 90 - Ampiezza;
      pos[1] = 90;//*
      pos[2] = 81;
      pos[3] = 54;
      pos[4] = 99;
      pos[5] = 126;
      pos[6] = 90 + Ampiezza;
      pos[7] = 54;  
      setServoWalk(); 
      if (MODALITA != MODALITA2) { return; }      
}


void rightWalk()
{
      //passo 1      
      pos[0] = 81;
      pos[1] = 126;
      pos[2] = 99;
      pos[3] = 54;  
      pos[4] = 90 + Ampiezza;
      pos[5] = 90;//*
      pos[6] = 90 - Ampiezza;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }      

      
      //passo 2      
      pos[0] = 90 - Ampiezza;
      pos[1] = 126;
      pos[2] = 81;
      pos[3] = 54;
      pos[4] = 99;
      pos[5] = 126;
      pos[6] = 90 + Ampiezza;
      pos[7] = 90;//*  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }

      
      //passo 3      
      pos[0] = 90 + Ampiezza;
      pos[1] = 90;//*
      pos[2] = 90 - Ampiezza;
      pos[3] = 54;
      pos[4] = 81;
      pos[5] = 126;
      pos[6] = 99;
      pos[7] = 54;   
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }

      
      //passo 4      
      pos[0] = 99;
      pos[1] = 126;
      pos[2] = 90 + Ampiezza;
      pos[3] = 90;//*
      pos[4] = 90 - Ampiezza;
      pos[5] = 126;
      pos[6] = 81;
      pos[7] = 54;  
      setServoWalk();
      if (MODALITA != MODALITA2) { return; }      
} 
void setServoWalk() 
{
   MODALITA2 = MODALITA;  
   for (i=1; i<=18; i++) {
       servo0.write(posVecchia[0] + i*(pos[0]-posVecchia[0])/18);          
       servo2.write(posVecchia[2] + i*(pos[2]-posVecchia[2])/18);       
       servo4.write(posVecchia[4] + i*(pos[4]-posVecchia[4])/18);       
       servo6.write(posVecchia[6] + i*(pos[6]-posVecchia[6])/18);  
 
      // servi delle zampe
         if ( i <= 9 ) 
           servo1.write(posVecchia[1] + i*(pos[1]-posVecchia[1])/9);         
         else 
           servo1.write(posVecchia[1] + (18-i)*(pos[1]-posVecchia[1])/9);      
 
         if ( i <= 9 ) 
           servo3.write(posVecchia[3] + i*(pos[3]-posVecchia[3])/9);                   
         else   
           servo3.write(posVecchia[3] + (18-i)*(pos[3]-posVecchia[3])/9); 
 
         if ( i <= 9 ) 
           servo5.write(posVecchia[5] + i*(pos[5]-posVecchia[5])/9);         
         else 
           servo5.write(posVecchia[5] + (18-i)*(pos[5]-posVecchia[5])/9);    
 
         if ( i <= 9 ) 
           servo7.write(posVecchia[7] + i*(pos[7]-posVecchia[7])/9);         
         else 
           servo7.write(posVecchia[7] + (18-i)*(pos[7]-posVecchia[7])/9); 
           
          ricevi(); 
          // riceviSeriale();
          if (MODALITA != MODALITA2) {
            
 
          posVecchia[0]=posVecchia[0] + i*(pos[0]-posVecchia[0])/18; 
          posVecchia[2]=posVecchia[2] + i*(pos[2]-posVecchia[2])/18; 
          posVecchia[4]=posVecchia[4] + i*(pos[4]-posVecchia[4])/18;
          posVecchia[6]=posVecchia[6] + i*(pos[6]-posVecchia[6])/18;
          if ( i <= 9 )
          {
            posVecchia[1] = posVecchia[1] + i*(pos[1]-posVecchia[1])/9;
            posVecchia[3] = posVecchia[3] + i*(pos[3]-posVecchia[3])/9; 
            posVecchia[5] = posVecchia[5] + i*(pos[5]-posVecchia[5])/9;
            posVecchia[7] = posVecchia[7] + i*(pos[7]-posVecchia[7])/9;
          } 
            
          else
          {
            posVecchia[1] = posVecchia[1] + (18-i)*(pos[1]-posVecchia[1])/9;
            posVecchia[3] = posVecchia[3] + (18-i)*(pos[3]-posVecchia[3])/9;
            posVecchia[5] = posVecchia[5] + (18-i)*(pos[5]-posVecchia[5])/9;
            posVecchia[7] = posVecchia[7] + (18-i)*(pos[7]-posVecchia[7])/9;
          }                       
          return; 
          }
       delay(Tempo/4/18);
    }   
    posVecchia[0]=pos[0];
    posVecchia[1]=126;//leg[1]; 
    posVecchia[2]=pos[2];
    posVecchia[3]=54;//leg[3];  
    posVecchia[4]=pos[4];
    posVecchia[5]=126;//leg[5]; 
    posVecchia[6]=pos[6];
    posVecchia[7]=54; //leg[7];
    ricevi();
    //riceviSeriale();

    
}  


void Esecuzione() 
{   
   for (i=0; i<18; i++) {
       servo0.write(posVecchia[0] + i*(pos[0]-posVecchia[0])/18);  
       servo1.write(posVecchia[1] + i*(pos[1]-posVecchia[1])/18);        
       servo2.write(posVecchia[2] + i*(pos[2]-posVecchia[2])/18);  
       servo3.write(posVecchia[3] + i*(pos[3]-posVecchia[3])/18);      
       servo4.write(posVecchia[4] + i*(pos[4]-posVecchia[4])/18);  
       servo5.write(posVecchia[5] + i*(pos[5]-posVecchia[5])/18);        
       servo6.write(posVecchia[6] + i*(pos[6]-posVecchia[6])/18);  
       servo7.write(posVecchia[7] + i*(pos[7]-posVecchia[7])/18);    
       delay(Tempo/4/18);
    }   
    posVecchia[0]=pos[0];
    posVecchia[1]=pos[1]; 
    posVecchia[2]=pos[2];
    posVecchia[3]=pos[3];  
    posVecchia[4]=pos[4];
    posVecchia[5]=pos[5]; 
    posVecchia[6]=pos[6];
    posVecchia[7]=pos[7];       
} 

void Esecuzione2() 
{   
   for (i=0; i<18; i++) {
       servo0.write(posVecchia[0] + i*(pos[0]-posVecchia[0])/18);  
       servo1.write(posVecchia[1] + i*(pos[1]-posVecchia[1])/18);        
       servo2.write(posVecchia[2] + i*(pos[2]-posVecchia[2])/18);  
       servo3.write(posVecchia[3] + i*(pos[3]-posVecchia[3])/18);      
       servo4.write(posVecchia[4] + i*(pos[4]-posVecchia[4])/18);  
       servo5.write(posVecchia[5] + i*(pos[5]-posVecchia[5])/18);        
       servo6.write(posVecchia[6] + i*(pos[6]-posVecchia[6])/18);  
       servo7.write(posVecchia[7] + i*(pos[7]-posVecchia[7])/18);    
       delay(TempoSeduto/4/18);
    }   
    posVecchia[0]=pos[0];
    posVecchia[1]=pos[1]; 
    posVecchia[2]=pos[2];
    posVecchia[3]=pos[3];  
    posVecchia[4]=pos[4];
    posVecchia[5]=pos[5]; 
    posVecchia[6]=pos[6];
    posVecchia[7]=pos[7];       
} 

void trasmitRequest(byte D7,byte D6,byte D5,byte D4,byte D3,byte D2,byte D1,byte D0,byte v1,byte v2,byte v3,byte v4, byte v5, byte v6, byte v7, byte v8){

  Serial1.write(0x7E);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x16);
  Serial1.write((byte)0x10);
  Serial1.write((byte)0x01);  
  Serial1.write((byte)D7);
  Serial1.write((byte)D6);
  Serial1.write((byte)D5);
  Serial1.write((byte)D4); 
  Serial1.write((byte)D3);
  Serial1.write((byte)D2); 
  Serial1.write((byte)D1); 
  Serial1.write((byte)D0); 
  Serial1.write((byte)0xFF); 
  Serial1.write((byte)0xFE);  
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)v1);
  Serial1.write((byte)v2);
  Serial1.write((byte)v3);
  Serial1.write((byte)v4); 
  Serial1.write((byte)v5);
  Serial1.write((byte)v6); 
  Serial1.write((byte)v7); 
  Serial1.write((byte)v8); 
 
  int sum=0x10+0x01+D7+D6+D5+D4+D3+D2+D1+D0+0xFF+(0xFE)+v1+v2+v3+v4+v5+v6+v7+v8;

  Serial1.write((byte)(0xFF-(sum&0xFF)));

  delay(10);
  
}

void ricevi(){
  
  byte lunM,
       lunL,
       checksum,
       datArray[30];
       
  int lunghezza;
 if(Serial1.available()>=1) {
   
  if(Serial1.read()==0x7E) {
    while(Serial1.available() <2){  
    }      
    lunM = Serial1.read();
    lunL = Serial1.read();
    lunghezza=(int)(lunL+(lunM<<8));       
    while(Serial1.available() <lunghezza){  
    }        
    for(int i=0;i<lunghezza;i++){
      datArray[i]=Serial1.read();
    }    
    while(Serial1.available() <1){  
    }
    checksum=Serial1.read();
    
    switch(datArray[0]) {         
      case 0x90:    
      
        float valore;
        switch(datArray[12]){
          case 0:
              Seduto();           
              break;
              
          case 1:
              switch(datArray[13]){
                case 0:
                    inStandby();
                    break;
                case 1:          
                    inPiedi();
                    break;
                    }
              break;
                  
          case 2:
              inWalk();
              break;
              
          case 3:
              inRight();
              break;
              
         case 4:
             inLeft(); 
             break;
             
        case 5:
             inReverseWalk();
             break;
        
        case 6:
             Ampiezza = datArray[13];
             break;
        
        case 7:
             C.b[0] = datArray[13];
             C.b[1] = datArray[14];
             Tempo = C.i;
             break;
        case 12:
             inGinocchio();
             break;
            
                     
   }
   delay(50);
  }
}
}
}
 

