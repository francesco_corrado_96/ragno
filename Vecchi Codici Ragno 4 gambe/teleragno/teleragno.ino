//-----------------------------------------------------------------------------
// librerie utilizzate
//-----------------------------------------------------------------------------
#include <Wire.h>   //libreria I2C
#include <SPI.h>
#include <SD.h>
#include <stdlib.h>

//-----------------------------------------------------------------------------
//dichiarazione delle costanti
//-----------------------------------------------------------------------------
#define DS1307_ADDRESS 0x68	//indirizzo I2C del RTC DS1307
#define NUM_XBEE    8           //numero dei moduli XBee 
#define TEMPO_SCAN  500         //tempo fra una richiesta dati di un modulo XBee e l'altro (mSec) 
 
//-----------------------------------------------------------------------------
// variabili globali
//-----------------------------------------------------------------------------
boolean DEBUG = false;  //1 visualizza su terminale 0 non visualizza (per debug)

byte ii,          // numero di nodo per richiesta ciclica  dati
     indiceIND,   // numero di nodo ricavato dall'indirizzo a 64 bit ricevuto
     zero=0x00;   // variabile con zero per gestione RTC
int temp; //per int da due byte;  
             
unsigned long tempoAttuale,       //tempo in millisecondi per temporizzazione
              tempoPrecedente;    //memoria della lettura precedente del tempo
              
union float4byte { // unione per scomporre un float in quattro byte e viceversa
 float f;   
 byte b[4];
} f4b;

union int2byte {   // unione per scomporre un intero in due byte e viceversa
 int i;   
 byte b[2];
} i2b;

typedef byte ADDR64[8];     //vettore di 8 byte contenente l'indirizzo Hardware del modulo Xbee
typedef byte DATO8[8];      //vettore per indirizzo o blocco dati a 8 byte;

typedef struct {  //struttura composta da un byte di informazione e dall'indirizzo a 8 byte del modulo Xbee
    byte tipo;
    ADDR64 ad64;
} TADR;

TADR a[9];    //vettore di 10 elementi con tipo e indirizzo dei nodi Xbee

File myFile;

//-----------------------------------------------------------------------------
// dichiarazione delle funzioni
//-----------------------------------------------------------------------------
void richiedi();
void ricevi();
void appello();

void trasmitRequest(TADR V,byte value);
void trasmitRequest8(TADR V,byte da1,byte da2,byte da3,byte da4,byte da5,byte da6,byte da7,byte da8);
void stampaAD64(TADR V);
byte confrontaAD64(TADR V1,TADR V2);
byte ricercaAD64(TADR V);

void daSER();

void setDateTime();
void printDate();
byte decToBcd(byte val);
byte bcdToDec(byte val);

//-----------------------------------------------------------------------------
// inizializzazioni
//-----------------------------------------------------------------------------
void setup() {
 Serial.begin(9600);    //inizializzazione della seriale 0 per debug
 Serial1.begin(9600);   //inizializzazione della seriale 1 (modulo Xbee)
 Wire.begin();          //inizializzazione del modulo I2C 
 
  
// inizializzazione struttura dati con tipo e indirizzo
 a[0].tipo=0;
 a[0].ad64[0]=0x00;  //indirizzo broadcast (indice array 0)
 a[0].ad64[1]=0x00;
 a[0].ad64[2]=0x00;
 a[0].ad64[3]=0x00;
 a[0].ad64[4]=0x00;
 a[0].ad64[5]=0x00;
 a[0].ad64[6]=0xFF;
 a[0].ad64[7]=0xFF;

 a[1].tipo=1;  
 a[1].ad64[0]=0x00;  //indirizzo router API 1
 a[1].ad64[1]=0x13;
 a[1].ad64[2]=0xA2;
 a[1].ad64[3]=0x00;
 a[1].ad64[4]=0x40;
 a[1].ad64[5]=0x8C;
 a[1].ad64[6]=0xCA;
 a[1].ad64[7]=0xAD;
 
 a[2].tipo=1; 
 a[2].ad64[0]=0x00;  //indirizzo router API 2
 a[2].ad64[1]=0x13;
 a[2].ad64[2]=0xA2;
 a[2].ad64[3]=0x00;
 a[2].ad64[4]=0x40;
 a[2].ad64[5]=0x8C;
 a[2].ad64[6]=0xCB;
 a[2].ad64[7]=0x5F;
 
 a[3].tipo=1;
 a[3].ad64[0]=0x00;  //indirizzo router API 3
 a[3].ad64[1]=0x13;
 a[3].ad64[2]=0xA2;
 a[3].ad64[3]=0x00;
 a[3].ad64[4]=0x40;
 a[3].ad64[5]=0xB4;
 a[3].ad64[6]=0x09;
 a[3].ad64[7]=0xB0;
 
 a[4].tipo=1;
 a[4].ad64[0]=0x00;  //indirizzo router API 4
 a[4].ad64[1]=0x13;
 a[4].ad64[2]=0xA2;
 a[4].ad64[3]=0x00;
 a[4].ad64[4]=0x40;
 a[4].ad64[5]=0xB4;
 a[4].ad64[6]=0x15;
 a[4].ad64[7]=0xFA;
 
 a[5].tipo=1;
 a[5].ad64[0]=0x00;  //indirizzo router API 5
 a[5].ad64[1]=0x13;
 a[5].ad64[2]=0xA2;
 a[5].ad64[3]=0x00;
 a[5].ad64[4]=0x40;
 a[5].ad64[5]=0xB4;
 a[5].ad64[6]=0x17;
 a[5].ad64[7]=0xC4;

 a[6].tipo=1;
 a[6].ad64[0]=0x00;  //indirizzo router API 6
 a[6].ad64[1]=0x13;
 a[6].ad64[2]=0xA2;
 a[6].ad64[3]=0x00;
 a[6].ad64[4]=0x40;
 a[6].ad64[5]=0xA8;
 a[6].ad64[6]=0x35;
 a[6].ad64[7]=0xD0;

 a[7].tipo=1;
 a[7].ad64[0]=0x00;  //indirizzo router API 7
 a[7].ad64[1]=0x13;
 a[7].ad64[2]=0xA2;
 a[7].ad64[3]=0x00;
 a[7].ad64[4]=0x40;
 a[7].ad64[5]=0xB1;
 a[7].ad64[6]=0xCB;
 a[7].ad64[7]=0x4B;

 a[8].tipo=1;
 a[8].ad64[0]=0x00;  //indirizzo router API 8
 a[8].ad64[1]=0x13;
 a[8].ad64[2]=0xA2;
 a[8].ad64[3]=0x00;
 a[8].ad64[4]=0x40;
 a[8].ad64[5]=0xB4;
 a[8].ad64[6]=0x09;
 a[8].ad64[7]=0xDF;
/*
 //inizializzazione SD card
 if (!SD.begin(4)) {
   Serial.println("initializzzazione SD fallita");
   return;
 }
  Serial.println("initializzazione SD eseguita");

 DEBUG=true; 
// printDate();   //ricavo l'ora ed il giorno
 DEBUG=false;
*/

 tempoPrecedente=millis();  //lettura tempo in miilisecondi
 ii=0; 

 Serial.println("PROVA RETE XBEE");
 
 ricevi();      //ricevi i dati dai moduli Xbee
 
}

//-----------------------------------------------------------------------------
// ciclo principale
//-----------------------------------------------------------------------------
void loop() {
//  richiedi();    //richiedi i dati ai moduli Xbee
  daSER();       //mediante terminale comanda i moduli Xbee e modifica i parametri al posto del WEB
  ricevi();      //ricevi i dati dai moduli Xbee

}

//-----------------------------------------------------------------------------
// richiesta temporizzata, ogni TEMPO_SCAN millisecondi richiede dati ad un 
// modulo Xbee cinque moduli in 5*TEMPO_SCAN millisecondi
//-----------------------------------------------------------------------------
void richiedi() {
  tempoAttuale=millis(); //leggi tempo
  if((tempoAttuale-tempoPrecedente)>TEMPO_SCAN){ //se dalla lettura precedente sono passati almeno 3 secondi
    ii++;                          //prossimo modulo
    if (ii>NUM_XBEE) ii=1;         //se ultimo modulo ricomincia dal primo
    tempoPrecedente=tempoAttuale;  //e aggiorna il tempo della richiesta dati
    switch(ii){  //quale modulo
      case 1:                        //modulo Router API 1
        trasmitRequest(a[1],80);     //invia comando richiesta dati  (80 decimale)   
        break;
      case 2:                        //modulo Router API 2
        trasmitRequest(a[2],80);     //invia comando richiesta dati  (80 decimale)
        break;
      case 3:                        //modulo Router API 3
        trasmitRequest(a[3],80);     //invia comando richiesta dati  (80 decimale)
        break;
      case 4:                        //modulo Router API 4
        trasmitRequest(a[4],80);     //invia comando richiesta dati  (80 decimale)
        break;
      case 5:                        //modulo Router API 5
        trasmitRequest(a[5],80);     //invia comando richiesta dati  (80 decimale)
        break;
       case 6:                        //modulo Router API 6
        trasmitRequest(a[6],80);     //invia comando richiesta dati  (80 decimale)
        break;
       case 7:                        //modulo Router API 7
        trasmitRequest(a[7],80);     //invia comando richiesta dati  (80 decimale)
        break;
       case 8:                        //modulo Router API 8
        trasmitRequest(a[8],80);     //invia comando richiesta dati  (80 decimale)
        break;
     default:;
    }
  }
}  

//----------------------------------------------------------------------------- 
// mediante terminale invia comandi ai moduli Xbee per verifica funzionamento  
//-----------------------------------------------------------------------------
void daSER(){

  if(Serial.available()>=1) {
      switch(Serial.read()) {
        case '0':                        //modulo Router API 1
          trasmitRequest(a[3],0);     //invia comando richiesta dati  (80 decimale)   
          break;
        case '1': //richiesta dati router API 1 
	  trasmitRequest(a[3],1);
          break;
        case '2': //richiesta dati router API 2
	  trasmitRequest(a[3],2);
          break;
        case '3': //richiesta dati router API 3 
     	  trasmitRequest(a[3],3);
          break;
        case '4': // richiesta dati router API 4
      	  trasmitRequest(a[3],4);
          break;
        case '5': // richiesta dati router API 5
	  trasmitRequest(a[3],5);
          break;
        case '6': // richiesta dati router API 6
	  trasmitRequest(a[6],80); 
          break;
        case '7': // richiesta dati router API 7
	  trasmitRequest(a[7],80); 
          break;
        case '8': // richiesta dati router API 8
	  trasmitRequest(a[8],80); 
          break;      
        case 'q': // set DEBUG
          DEBUG=true;  
          break;
        case 'w': // reset DEBUG 
          DEBUG=false;  
          break;
        case 'e': // appello API device  
          appello();      
          break;
        case 'a': // setta ora e data 
          setDateTime();
          break;
        case 's': // legge ora e data e visualizza
          printDate();
          break;  
        case 'z': // scrive SD card
          // open the file. note that only one file can be open at a time,
          // so you have to close this one before opening another.
          myFile = SD.open("test.txt", FILE_WRITE);
          // if the file opened okay, write to it:
          if (myFile) {
            Serial.print("Writing to test.txt...");
            myFile.println("testing 1, 2, 3.");
            // close the file:
            myFile.close();
            Serial.println("done.");
          } else {
            // if the file didn't open, print an error:
            Serial.println("error opening test.txt");
          }          
          break;
        case 'x': // legge SD card
          // open the file for reading:
          myFile = SD.open("test.txt");
          if (myFile) {
            Serial.println("test.txt:");   
            // read from the file until there's nothing else in it:
            while (myFile.available()) {
              Serial.write(myFile.read());
            }
            // close the file:
            myFile.close();
          } else {
            // if the file didn't open, print an error:
            Serial.println("error opening test.txt");
          }         
          break;          
        case 'n': // va a capo 
 	  Serial.println();
          break;
        default:
          break;
      }
  }
} 

//-----------------------------------------------------------------------------  
// riceve da seriale 1 collegata al modulo XBee le informazioni e i dati 
//-----------------------------------------------------------------------------
void ricevi(){
  
  byte  lunM,         //byte piu significativo della lunghezza del frame
        lunL,         //byte meno significativo della lunghezza del frame
        checksum,     //valore del checksum ricevuto
        datArray[30]; //vettore in cui vanno i byte letti dopo la lettura della lunghezza del frame
  
  int lunghezza;      //lunghezza del frame
  
  TADR D64;           //struttura con indirizzo a 8 byte (64 bit) e tipo
  
 if(Serial1.available()>=1) {        //se vi sono dei dati ricevuti dalla seriale1
      
  if(Serial1.read()==0x7E) {         //se il primo byte e l'identificativo del frame XBee
    while(Serial1.available() <2){   //aspetta che arrivino i prossimi due byte (lunghezza Frame)
    }      
    lunM = Serial1.read();  //e leggili
    lunL = Serial1.read();
    lunghezza=(int)(lunL+(lunM<<8)); //metti la lunghezza in un intero (due byte)    
    while(Serial1.available() <lunghezza){ //aspetta che arrivino un numero di byte pari alla lunghezza del frame
    }        
    for(int i=0;i<lunghezza;i++){  //leggi i byte arrivati e mettili nel vettore datArray
      datArray[i]=Serial1.read();
    }   
    while(Serial1.available() <1){  //aspetta che arrivi il byte del checksum
    }
    checksum=Serial1.read();        //e leggilo
     
    for(int i=1; i<9;i++){    
       D64.ad64[i-1]=datArray[i];    //metti in D64 (struttura di tipo TADR) l'indirizzo a 8 byte del modulo XBee
    }
    indiceIND=ricercaAD64(D64);      //ricava l'indice del vettore (struttura di tipo TADR) con gli indirizzi a 8 byte dei moduli XBee
   
    switch(datArray[0]) {            //tipo Frame ricevuto
      case 0x8B:                     //Zig Bee Trasmit Status (p. 111)
      	if(DEBUG){                   //se visualizzazione abilitata
          Serial.print(datArray[0],HEX);//visualizzo il tipo di frame
          Serial.print(' ');
          Serial.print(datArray[5]); //visualizzo Delivery Status
          Serial.print(' ');        
          Serial.println(datArray[6]);//visualizzo Discovery Status  
        }
        break;      
      case 0x90:                     //ZigBee Receive Packet (p. 112)
        switch(indiceIND){           //in base all'indice del vettore (struttura di tipo TADR) con indirizzo a 8byte
          case 1:                    //modulo XBee API 1          
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API1  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');   
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	       Serial.print(' ');             
              Serial.print(datArray[13]);       
 	       Serial.print(' ');             
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       
*/
 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
            }           
            break;
          case 2:                    //modulo XBee API 2              
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API2  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');       
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
 	       Serial.print(' ');             
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);   
*/    
 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
           }           
            break;
          case 3:                    //modulo XBee API 3                 
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("CORR  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');   
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);  
   	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);  
*/
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/*              
 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(':');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print('/');             
              Serial.println(datArray[19]);   
*/              
              temp=datArray[17];
              temp=temp<<8;
              temp=temp | datArray[16];
 	      Serial.print(' ');             
              Serial.print(temp);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	      Serial.print('/');             
              Serial.println(datArray[19]);  
            }           
            break;
          case 4:                    //modulo XBee API 4                 
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API4  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' '); 
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
   	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);   
*/    
 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
            }           
            break;
          case 5:                    //modulo XBee API 5               
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API5  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' '); 
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
    	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       

*/ 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
           }           
            break;
          case 6:                    //modulo XBee API 6                 
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("GRAN  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');  
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.println(datArray[13]);       
   	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       

 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]); 
 */             
              temp=datArray[17];
              temp=temp<<8;
              temp=temp | datArray[16];
 	      Serial.print(' ');             
              Serial.print(temp);       
  	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	      Serial.print('/');             
              Serial.println(datArray[19]);  
         
              
           }           
            break;
          case 7:                    //modulo XBee API 7               
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API7  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');  
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
    	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       

*/ 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
           }           
            break;
          case 8:                    //modulo XBee API 8               
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("API8  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');   
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
    	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       
*/
 	       Serial.print(' ');             
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
           }           
            break;

          default:
            if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
              Serial.print("SCONOSCIUTO  ");
              Serial.print(datArray[11]);//Receive Option
 	      Serial.print(' ');  
               f4b.b[0]=datArray[12]; 
               f4b.b[1]=datArray[13]; 
               f4b.b[2]=datArray[14]; 
               f4b.b[3]=datArray[15]; 
               Serial.print(f4b.f);
/* 
              Serial.print(datArray[12]);       
 	      Serial.print(' ');             
              Serial.print(datArray[13]);       
    	      Serial.print(' ');       
              Serial.print(datArray[14]);       
 	       Serial.print(' ');             
              Serial.print(datArray[15]);       
 	       Serial.print(' ');             
*/
              Serial.print(datArray[16]);       
 	       Serial.print(' ');             
              Serial.print(datArray[17]);       
 	       Serial.print(' ');             
              Serial.print(datArray[18]);       
 	       Serial.print(' ');             
              Serial.println(datArray[19]);       
           }           
            break;
        }        
        break;
      case 0x92:                   //ZigBee IO Data Sample RX Indicator (p. 114)
        switch(indiceIND){         //in base all'indice del vettore (struttura di tipo TADR) con indirizzo a 8byte
          case 1:                  //modulo XBee AT mettere quelli che vi sono nella rete

            break;

          default:;
        }   
        break;
      case 0x97:                //Remote Command Response (p. 118)
      	if(DEBUG){              //se visualizzazione abilitata
          Serial.print(datArray[0],HEX);//visualizzo il tipo di frame
          Serial.print(' ');
          Serial.println(datArray[14]); //ed il Command Status
        }
        break;        
      default :                 //altro tipo di frame
      	if(DEBUG){              //se visualizzazione abilitata
          Serial.print(datArray[0],HEX); //visualizzo il tipo di frame
          Serial.print(' ');
          Serial.println("NON GESTITO");
        }
    }
   }
   else {                      //non e' un frame di una trasmissione XBee
      	if(DEBUG){             //se visualizzazione abilitata
          Serial.println('*'); //visualizza un asterisco   
      	}   
   }
  }
}


//-----------------------------------------------------------------------------
// richiede i dati a tutti i moduli Xbee e quindi anche gli indirizzi a 8 byte 
//-----------------------------------------------------------------------------
void appello(){
  trasmitRequest(a[0],80);     //invia comando richiesta dati a tutti  (80 decimale)  	       
}


//-----------------------------------------------------------------------------
// comando di trasmissione remota (Frame 0x10) con indirizzo come struttura
// di tipo TADR e un dato di un byte
//-----------------------------------------------------------------------------
void trasmitRequest(TADR V,byte value){
  Serial1.write(0x7E);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x16);
  Serial1.write((byte)0x10);
  Serial1.write((byte)0x01);  
  Serial1.write((byte)V.ad64[0]);
  Serial1.write((byte)V.ad64[1]);
  Serial1.write((byte)V.ad64[2]);
  Serial1.write((byte)V.ad64[3]); 
  Serial1.write((byte)V.ad64[4]);
  Serial1.write((byte)V.ad64[5]); 
  Serial1.write((byte)V.ad64[6]); 
  Serial1.write((byte)V.ad64[7]); 
  Serial1.write((byte)0xFF); 
  Serial1.write((byte)0xFE);  
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)value);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0); 
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0); 
  Serial1.write((byte)0x0); 
  Serial1.write((byte)0x0); 
  int sum=0x10+0x01+V.ad64[0]+V.ad64[1]+V.ad64[2]+V.ad64[3]+V.ad64[4]+V.ad64[5]+V.ad64[6]+V.ad64[7]+0xFF+(0xFE)+value;
  Serial1.write((byte)(0xFF-(sum&0xFF)));
  delay(10); 
}

//-----------------------------------------------------------------------------
// comando di trasmissione remota (Frame 0x10) con indirizzo come struttura
// di tipo TADR e un dato di un byte
//-----------------------------------------------------------------------------
void trasmitRequest8(TADR V,byte da1,byte da2,byte da3,byte da4,byte da5,byte da6,byte da7,byte da8){
  Serial1.write(0x7E);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x16);
  Serial1.write((byte)0x10);
  Serial1.write((byte)0x01);  
  Serial1.write((byte)V.ad64[0]);
  Serial1.write((byte)V.ad64[1]);
  Serial1.write((byte)V.ad64[2]);
  Serial1.write((byte)V.ad64[3]); 
  Serial1.write((byte)V.ad64[4]);
  Serial1.write((byte)V.ad64[5]); 
  Serial1.write((byte)V.ad64[6]); 
  Serial1.write((byte)V.ad64[7]); 
  Serial1.write((byte)0xFF); 
  Serial1.write((byte)0xFE);  
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)da1);
  Serial1.write((byte)da2);
  Serial1.write((byte)da3);
  Serial1.write((byte)da4); 
  Serial1.write((byte)da5);
  Serial1.write((byte)da6); 
  Serial1.write((byte)da7); 
  Serial1.write((byte)da8); 
  int sum=0x10+0x01+V.ad64[0]+V.ad64[1]+V.ad64[2]+V.ad64[3]+V.ad64[4]+V.ad64[5]+V.ad64[6]+V.ad64[7]+0xFF+(0xFE)+da1+da2+da3+da4+da5+da6+da7+da8;
  Serial1.write((byte)(0xFF-(sum&0xFF)));
  delay(10); 
}


//-----------------------------------------------------------------------------
// visualizza l'indirizzo del modulo XBee in esadecimale (8 byte) 
//-----------------------------------------------------------------------------
void stampaAD64(TADR V){
  byte i;
  
  for(i=0;i<8;i++){
    Serial.print(V.ad64[i],HEX);
    Serial.print(' ');    
  }
}

//-----------------------------------------------------------------------------
// confronta due indirizzi a 8 byte dei moduli XBee
//-----------------------------------------------------------------------------
byte confrontaAD64(TADR V1,TADR V2){
  byte i,uguali=1;
  
  for(i=0;i<8;i++){
    if(V1.ad64[i]!=V2.ad64[i])
      uguali=0;
  }
  return uguali;
}

//-----------------------------------------------------------------------------
// ricerca e restituisce l'indice nel vettore struttura TADR con indirizzo 
// passato come parametro
//-----------------------------------------------------------------------------
byte ricercaAD64(TADR V){
  byte i,k,uguali;

  k=1;
  while(a[k].tipo!=0){
    uguali=1;
    for(i=0;i<8;i++){
      if(V.ad64[i]!=a[k].ad64[i])
        uguali=0;
    }
    if(uguali==1) return k;
    k++;
  }
  return 0;
}



//-----------------------------------------------------------------------------
// converte da decimale a binary coded decimal (BCD)
//-----------------------------------------------------------------------------
byte decToBcd(byte val){
  return ( (val/10*16) + (val%10) );
}

//-----------------------------------------------------------------------------
// convert da binary coded decimal (BCD) a decimale
//-----------------------------------------------------------------------------
byte bcdToDec(byte val)  {
  return ( (val/16*10) + (val%16) );
}

//-----------------------------------------------------------------------------
// setta la data nel RTC DS1307
//-----------------------------------------------------------------------------
void setDateTime(){

  byte second =      00; //0-59
  byte minute =      45; //0-59
  byte hour =        19; //0-23
  byte weekDay =     02; //1-7
  byte monthDay =    25; //1-31
  byte month =       02; //1-12
  byte year  =       13; //0-99

  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero); //stop Oscillator
  
  Wire.write(decToBcd(second));
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour));
  Wire.write(decToBcd(weekDay));
  Wire.write(decToBcd(monthDay));
  Wire.write(decToBcd(month));
  Wire.write(decToBcd(year));
  
  Wire.write(zero); //start 
  
  Wire.endTransmission();
  
}

//-----------------------------------------------------------------------------
// legge la data e l'ora dal RTC DS1307
//-----------------------------------------------------------------------------
void printDate(){

  // Reset the register pointer
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero);
  Wire.endTransmission();
  
  Wire.requestFrom(DS1307_ADDRESS, 7);
  
  int second = bcdToDec(Wire.read());
  int minute = bcdToDec(Wire.read());
  int hour = bcdToDec(Wire.read() & 0b111111); //24 hour time
  int weekDay = bcdToDec(Wire.read()); //0-6 -> sunday - Saturday
  int monthDay = bcdToDec(Wire.read());
  int month = bcdToDec(Wire.read());
  int year = bcdToDec(Wire.read());
  
      if(DEBUG){              //se visualizzazione abilitata visualizzo i dati letti 
          Serial.print(monthDay);
          Serial.print("/");
          Serial.print(month);
          Serial.print("/");
          Serial.print(year);
          Serial.print(" ");
          Serial.print(hour);
          Serial.print(":");
          Serial.print(minute);
          Serial.print(":");
          Serial.println(second);   
      }
}

