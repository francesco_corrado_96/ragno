#include <OneWire.h>
#include <LiquidCrystal.h>
#include "Wire.h"
#include <EEPROM.h>
#define DS1307_ADDRESS 0x68
byte zero = 0x00; 

OneWire ds(10);
int second;
int minute;
int hour; 
int weekDay;
int monthDay;
int month;
int year;
int Vor = A1;
int contatore = 1;
int Puls2 = 8;
int Puls;
int hour1;
union TRIDATO{float f; byte b[4];};
TRIDATO C;
int Vvert = A0;
byte dato [4];
float X;
int Y;
int addr;
int K;
int P;
int address = 15;
float Vet[24];
float L = 0;
int I;
float celsius;
int H;
float Z;
int W;
int T = 0;
int Cambio2 = 13;
int Cambio;
LiquidCrystal lcd(12, 11, 5, 4, 7, 9);
union BIDATO{float f; byte b[4];};
BIDATO B;
const int TRIG_PIN = A2;
const int ECHO_PIN = A3;


void trasmitRequest(byte D7,byte D6,byte D5,byte D4,byte D3,byte D2,byte D1,byte D0,byte v1,byte v2,byte v3,byte v4,byte v5, byte v6, byte v7, byte v8);
void ricevi();


void setup(void) {
  Wire.begin();
  Serial1.begin(9600);
  //setDateTime(); 
  Serial.begin(9600);
  pinMode(8, INPUT);
  pinMode(13, OUTPUT);
  lcd.begin(16, 2);
  pinMode(13, INPUT);
  pinMode(TRIG_PIN,OUTPUT);
  pinMode(ECHO_PIN,INPUT);
}
/*
void setDateTime(){

  byte second =      06; //0-59
  byte minute =      18; //0-59
  byte hour =        11; //0-23
  byte weekDay =     6; //1-7
  byte monthDay =    30; //1-31
  byte month =       10; //1-12
  byte year  =       14; //0-99

  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero); //stop Oscillator

  Wire.write(decToBcd(second));
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour));
  Wire.write(decToBcd(weekDay));
  Wire.write(decToBcd(monthDay));
  Wire.write(decToBcd(month));
  Wire.write(decToBcd(year));

  Wire.write(zero); //start 

  Wire.endTransmission();
}

*/

byte decToBcd(byte val){
  return ( (val/10*16) + (val%10) );
}

byte bcdToDec(byte val)  {
  return ( (val/16*10) + (val%16) );
}

void printDate(){

  for(int x=0;x<24;x++)
  {    
     for(int i=0; i<4; i++)
     {
       B.b[i] = EEPROM.read(address+i);
     }
  Vet[x] = B.f;
  address = address+4;
  if (address>111) {address = 15;}
  }
  address = 15;
  
  Wire.beginTransmission(DS1307_ADDRESS);
  Wire.write(zero);
  Wire.endTransmission();

  Wire.requestFrom(DS1307_ADDRESS, 7);

  second = bcdToDec(Wire.read());
  minute = bcdToDec(Wire.read());
  hour = bcdToDec(Wire.read() & 0b111111); 
  weekDay = bcdToDec(Wire.read());
  monthDay = bcdToDec(Wire.read());
  month = bcdToDec(Wire.read());
  H = hour;
  year = bcdToDec(Wire.read());

    if (T == 1)
  {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(monthDay);
    lcd.setCursor(2,0);
    lcd.print("/");
    lcd.setCursor(3,0);
    lcd.print(month);
    lcd.setCursor(5,0);
    lcd.print("/");
    lcd.setCursor(6,0);
    lcd.print(year);
    lcd.setCursor(0,1);
    lcd.print(hour);
    lcd.setCursor(2,1);
    lcd.print(":");
    lcd.setCursor(3,1);
    lcd.print(minute);
    lcd.setCursor(5,1);
    lcd.print(":");
    lcd.setCursor(6,1);
    lcd.print(second);
  }

  if (hour != hour1)
  {
    X = Vet[hour];
  } 
}



void loop(void) {
  
  long duration, distanceCm;
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  duration = pulseIn(ECHO_PIN,HIGH);
  distanceCm = duration / 29.1 / 2 ;

  
  printDate();
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];

  
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(150);
    return;
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        
  delay(50);     
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         

  for ( i = 0; i < 9; i++)
  {          
    data[i] = ds.read();
  }
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s)
  {
    raw = raw << 3;
    if (data[7] == 0x10)
    {
       raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } 
  else 
  {
    byte cfg = (data[4] & 0x60);
    if (cfg == 0x00) raw = raw & ~7;  
    else if (cfg == 0x20) raw = raw & ~3; 
    else if (cfg == 0x40) raw = raw & ~1; 
  }
  
  celsius = (float)raw / 16.0;

  

  Y = analogRead(Vvert);
  Puls = digitalRead (Puls2);
 if (Puls == 0)
 {
  W = !W;
 }
 if (W == 1)
 {
  if ( Y >= 1000 )
    {
    X=X-0.5;
    delay(50);
    }
  if (Y <= 20 )
    {
    X=X+0.5;
    delay(50);
    }
  }
  
  Vet[H] = X;
  
  for(int x=0; x<24; x++)
  {
  B.f = Vet[x];
  for(int i=0; i<4;i++)
    {
    EEPROM.write(address+i, B.b[i]);
    }
  address = address + 4;
 }  
 

  Cambio = digitalRead(Cambio2);
  if (Cambio == 0)
  {
    T++;
    delay(50);
    if(T > 3)
    {
      T = 0;
    }
  }  
  if (T == 0)
  {
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("T=");
  lcd.setCursor(2,0);
  lcd.print(celsius);
  lcd.setCursor(0,1);
  lcd.print("Ts=");
  lcd.setCursor(3,1);
  lcd.print(X);
  lcd.setCursor(7,1);
  lcd.print(" ");
  if(W==0)
    {
    lcd.setCursor(13,1);
    lcd.print("   ");
    lcd.setCursor(13,1);
    lcd.print("OFF");
    }
  else
    {
    lcd.setCursor(13,1);
    lcd.print("   ");
    lcd.setCursor(13,1);
    lcd.print("ON");
    }
  }
  
  if (T == 2)
  {
    
    if (analogRead(Vor) >= 1000)
    {
      contatore ++;
      if (contatore > 9){contatore = 1;}
      if (contatore == 3){contatore = 4;}
      delay(150);
    }
    if (analogRead(Vor) <=20)
    {
      contatore --;
      if (contatore <1){contatore = 9;}
      if (contatore == 3){contatore = 2;}
      delay(150);
    } 
    lcd.setCursor(0,1);
    lcd.print("invia a ");
    lcd.setCursor(8,1);
    lcd.print ("gruppo:");
    

    if (contatore == 9)
    {
      lcd.setCursor(15,1);
      lcd.print("C");
    }
    if (contatore != 3 && contatore != 9)
    {
      lcd.setCursor(15,1);
      lcd.print(contatore);
    }
    
    ricevi(); 
    if(Puls == LOW)
    { 
      
      union QUADRADATO{
        long l;
        byte b[4];
      };
      QUADRADATO Q;
      
    C.f = celsius;
      for (int i = 0; i<4; i++)
       {
         dato[i] = C.b[i];
       }
      Q.l = distanceCm;
       
      
      switch(contatore){
        
        case 1:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0x8C,0xCA,0xAD,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 2:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0x8C,0xCB,0x5F,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 4:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0xB4,0x15,0xFA,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 5:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0xB4,0x17,0xC4,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 6:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0xA8,0x35,0xD0,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 7:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0xB1,0xCB,0x4B,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 8:
          delay(300);
          trasmitRequest(0x00,0x13,0xA2,0x00,0x40,0xB4,0x09,0xDF,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);
          break;
        case 9:
          delay(300);
          trasmitRequest(0x00,0x0,0x0,0x00,0x0,0x0,0x0,0x0,dato[0],dato[1],dato[2],dato[3],Q.b[0],Q.b[1],monthDay,month);
          break;
      }
    }
  }
  if (T == 3)
  {
  if (distanceCm <= 0){
    lcd.clear();
    lcd.setCursor(3, 0);
    lcd.print("Errore");
    delay(500);
  }
  else {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Distanza: ");
    lcd.setCursor(10, 0);
    lcd.print(distanceCm);
    lcd.setCursor(14, 0);
    lcd.print("Cm");
    delay(500); 
  }
  }

    hour1 = H;
}




void ricevi(){
  
  byte lunM,
       lunL,
       checksum,
       datArray[30];
       
  int lunghezza;
 if(Serial1.available ()==0){    
    lcd.setCursor(0,0);
    lcd.print("In ricezione...");
 } 
 if(Serial1.available()>=1) {
      
  if(Serial1.read()==0x7E) {
    while(Serial1.available() <2){  
    }      
    lunM = Serial1.read();
    lunL = Serial1.read();
    lunghezza=(int)(lunL+(lunM<<8));       
    while(Serial1.available() <lunghezza){  
    }        
    for(int i=0;i<lunghezza;i++){
      datArray[i]=Serial1.read();
    }    
    while(Serial1.available() <1){  
    }
    checksum=Serial1.read();
    
    switch(datArray[0]) {   
      case 0x8B:    // Zig Bee Trasmit Status p. 111  
        lcd.clear(); 
      
        lcd.setCursor(0, 0);
        lcd.print("Frame tipo: "); 
        lcd.setCursor(12, 0); 
        lcd.print(datArray[0],HEX);
        if(datArray[5] == 0 && datArray[6] == 0)
        {
          
          lcd.setCursor(0,1);
          lcd.print("Conferma invio");
          delay(200);
        }
        else
        {
          lcd.setCursor(0,1);
          lcd.print("Errore: ");
          lcd.setCursor(8,1);
          lcd.print(datArray[5], HEX);
          lcd.setCursor(11,1);
          lcd.print(datArray[6], HEX);
        }

 
        break;      
      case 0x90:    // ZigBee Receive Packet p. 112
      
        float valore;
        switch(datArray[12]){
          case 80:
            C.f = celsius;
            for (int i = 0; i<4; i++)
            {
              dato[i] = C.b[i];
            }
           trasmitRequest(0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,dato[0],dato[1],dato[2],dato[3],hour,minute,monthDay,month);  
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Richiesta di T");
            lcd.setCursor(0,1);
            lcd.print("Ricevuta");
            delay(700);
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Invio in corso");
            delay(500);

           delay(300);
           break;
        
        default:
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Ricevuto Dato");
          delay(1200);
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("indi: "); 
          lcd.setCursor(6, 0);
          lcd.print(datArray[5],HEX);
          lcd.setCursor(8, 0);
          lcd.print(datArray[6],HEX); 
          lcd.setCursor(10, 0);
          lcd.print(datArray[7],HEX);
          lcd.setCursor(12, 0);
          lcd.print(datArray[8],HEX); 
          for (int i=0;i<4;i++)
          {
            C.b[i] = datArray[12+i];
          }
          valore = C.f;
          lcd.setCursor(0, 1);
          lcd.print("dato :");
          lcd.setCursor(6, 1);
          lcd.print(valore);
          delay(4000);  
          break;    
   }
  }
}
}
}


void trasmitRequest(byte D7,byte D6,byte D5,byte D4,byte D3,byte D2,byte D1,byte D0,byte v1,byte v2,byte v3,byte v4, byte v5, byte v6, byte v7, byte v8){

  Serial1.write(0x7E);
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x16);
  Serial1.write((byte)0x10);
  Serial1.write((byte)0x01);  
  Serial1.write((byte)D7);
  Serial1.write((byte)D6);
  Serial1.write((byte)D5);
  Serial1.write((byte)D4); 
  Serial1.write((byte)D3);
  Serial1.write((byte)D2); 
  Serial1.write((byte)D1); 
  Serial1.write((byte)D0); 
  Serial1.write((byte)0xFF); 
  Serial1.write((byte)0xFE);  
  Serial1.write((byte)0x0);
  Serial1.write((byte)0x0);
  Serial1.write((byte)v1);
  Serial1.write((byte)v2);
  Serial1.write((byte)v3);
  Serial1.write((byte)v4); 
  Serial1.write((byte)v5);
  Serial1.write((byte)v6); 
  Serial1.write((byte)v7); 
  Serial1.write((byte)v8); 
 
  int sum=0x10+0x01+D7+D6+D5+D4+D3+D2+D1+D0+0xFF+(0xFE)+v1+v2+v3+v4+v5+v6+v7+v8;

  Serial1.write((byte)(0xFF-(sum&0xFF)));

  delay(10);
  
}
