#ifndef __RAGNO__
#define __RAGNO__

class Ragno {
  private:
    int n_gambe;
    int gambaPortante;
    enum {FERMO, IN_MOVIMENTO} stato;
    Gamba* listaGambe[MAX];

  public:
    Ragno(int n_gambe);
    int muovi(enum {AVANTI, INDIETRO} dir);
    int fermo();
    
}

#endif
