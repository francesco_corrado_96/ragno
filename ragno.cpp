#include "ragno.h"
#include "Arduino.h"

Ragno::Ragno(int n_gambe) {
  enum {DESTRO, SINISTRO} lato;
  this->n_gambe = n_gambe;
  this->gambaPortante = 0;
  this->stato = FERMO;
  for (int i = 0; i < n_gambe; i++) {
    if(i%2 == 0) lato = SINISTRO;
    else lato = DESTRO;
    listaGambe[i] = new Gamba(i, lato);
  }
}

int Ragno::muovi(enum {AVANTI, INDIETRO} dir)
{
  if(stato == FERMO) {
    stato = IN_MOVIMENTO;
    gamba[0].avvia(dir);
    gambaPortante = 0;
  } else {
    if (Gamba[gambaPortante].stato == 2) {
      gambaPortante++;
      if (gambaPortante > 5) gambaPortante = 0;
      gamba[gambaPortante].avvia(direzione);
      gambeInMovimento++;
    }
  }
  return 0;
}

int Ragno::fermo() {
  if (stato != FERMO) {
    stato = FERMO;
    for(int i = 0; i < 6; i++) {
      gamba[i].fermo();
    }
  }
  return 0;
}

