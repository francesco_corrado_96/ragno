#include "gamba.h"
#include "Arduino.h"

Gamba::Gamba(int nGamba, enum {DESTRO, SINISTRO} lato, int divergOrizz, int divergVert) {
  this->nGamba = nGamba;
  this->movimento = 0;
  switch (lato) {
    case DESTRO:
      this->actualPosOrizz = RIGHT_ORIZZ_DEFAULT + divergOrizz;
      this->actualPosVert = RIGHT_VERT_DEFAULT + divergVert;
      break;
    case SINISTRO:
      this->actualPosOrizz = LEFT_ORIZZ_DEFAULT + divergOrizz;
      this->actualPosVert = LEFT_VERT_DEFAULT + divergVert;
  }
  this->lato = lato;
}

int Gamba::avvia(enum {AVANTI, INDIETRO} dir) {
  stato = PRIMO;
  movimento = 30;
  while(stato != ATTESA) {
    muovi(dir);
  }
  return 0;
}

int Gamba::muovi(enum {AVANTI, INDIETRO} dir) {
  if (stato == PRIMO) {
    if(movimento > 0) {
      // implementazione dei servo
      movimento--;
    } else {
      stato = SECONDO;
      movimento = 100;
    }
  }
  if (stato == SECONDO) {
    if (movimento > 0) {
      // implementazione dei servo
      movimento--;
    } else {
      stato = ATTESA;
    }
  }
  return 0;
}

