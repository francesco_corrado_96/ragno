#ifndef __GAMBA__
#define __GAMBA__


class Gamba {
  private:
    int nGamba;
    int actualPosOrizz;
    int actualPosVert;
    int movimento;
    enum {DESTRO, SINISTRO} lato;
    enum {PRIMO, SECONDO, ATTESA} stato;

    int muovi(enum {AVANTI, INDIETRO} dir);
  public: 
    Gamba(int nGamba, enum {DESTRA, SINISTRA} lato);
    int avvia(enum {AVANTI, INDIETRO} dir);
    enum {PRIMO, SECONDO, ATTESA} getStato();
};

#endif

